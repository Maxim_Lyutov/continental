import pdfplumber, re, sys 
from os import path
from pandas import DataFrame

print('Конвертер pdf для Continental version 2.1 \n')

if len(sys.argv) > 1: 
    path_to_file = sys.argv[1]
    print(f"Обрабатываем {path_to_file}")
else:
    sys.exit(f" --- Справка ---:\nКоманда: [скрипт] [файл pdf] \n")

total1 = {}
df = DataFrame(total1)
head = ['Item','Material number','Description','Qty','Type','Unit Price','Amount','Country of origin', 'Net Weight (kg)', 'Invoice']
country, invoice , weight = '','' , []

with pdfplumber.open(path_to_file) as data_pdf:
    
    for page in data_pdf.pages:
        string = page.extract_text().encode(encoding='ascii', errors='ignore').decode()
        match_weight = re.findall(r"(Net Weight \(kg\):\s+)([.\d]+)", string)
        if match_weight :
            for match in match_weight:
                weight.extend([match[1]])

    weight_index = 0
    rows = []
    for page in data_pdf.pages:
        string = page.extract_text().encode(encoding='ascii', errors='ignore').decode()
        res = re.search('(Invoice Number\s\/\s\w+\s)(\d+)', string)
        if invoice == '' and res : invoice = res.group(2)
        #print(string) 
        #matchs  = re.findall(r"(\d{1,5})\s+([-\dA-z]+)\s+[*]{0,4}([A-z\s,]+)\s+[\d]+\s(\d{1,4})\s([A-z]+)\s([\d.]+)\s([\d.,]+)\n([A-z]+)", string)
        matchs  = re.findall(r"\n(\d{1,5})\s+([-\dA-z]+)\s+[*]{0,4}([.&A-z\s,\d-]+)\s+[\d]+\s(\d{1,4})\s([A-z]+)\s([\d.]+)\s([\d.,]+)\n([A-z]+)", string)
        if matchs :
            for match in matchs:
                rows.extend([*match, weight[weight_index], invoice])
                print(*rows)
                weight_index += 1
                for item in range(len(head)):
                    total1.setdefault(head[item], []).append(rows[item])
                rows = []
            
            df = DataFrame(total1)
            country = ''
            
excel_name = path.basename(path_to_file).replace('.','_')

df.to_excel(f"./{excel_name}.xlsx", sheet_name=excel_name, index=False )
print('Excel создан.')
